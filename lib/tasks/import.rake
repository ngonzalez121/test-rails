require_relative '../import/gtfs'

namespace :data do
  desc "Import GTFS file URL"
  task :import, [:file_url] => :environment do |t, args|
    Import::Gtfs.new(args[:file_url]).import
  rescue => _exception
    Rails.logger.error _exception
  end

  desc "Set stops as deleted"
  task :cleanup, [:file_url] => :environment do |t, args|
    Import::Gtfs.new(args[:file_url]).cleanup
  rescue => _exception
    Rails.logger.error _exception
  end

  desc "Delete stops marked as deleted"
  task :delete => :environment do
    ActiveRecord::Base.connection.execute "DELETE FROM #{StopArea.table_name} WHERE deleted_at IS NOT NULL;"
  end

  desc "Flush queue"
  task :flush_queue => :environment do
    RedisDb.client.flushall
  end
end

namespace :download do
  desc "Download a large GTFS sample"
  task :big_sample do
    sh "curl -L -o gtfs.zip https://transitfeeds.com/p/rejseplanen/705/latest/download"
  end

  desc "Download a small GTFS sample"
  task :small_sample do
    sh "curl -L -o gtfs.zip https://developers.google.com/transit/gtfs/examples/sample-feed.zip"
  end
end
