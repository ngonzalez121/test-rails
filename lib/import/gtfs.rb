module Import
  class Gtfs
    attr_accessor :source

    def initialize(url)
      @source = GTFS::Source.build(url)
    rescue StandardError => _exception
      Rails.logger.error _exception
      raise Error.new("Failed to import GTFS file")
    end

    def import
      source.stops.each_slice(100) do |stops|
        ImportWorker.perform_async(stops.to_json)
      end
    end

    def cleanup
      stops_ids = source.stops.map(&:id)
      CleanupWorker.perform_async(stops_ids.to_json)
    end

  end
end
