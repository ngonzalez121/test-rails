RSpec.describe StopArea do
  subject(:stop_area) { StopArea.new }

  describe "#valid?" do
    subject { stop_area.valid? }

    context "when name is empty" do
      before { stop_area.name = nil }
      it { is_expected.to be_falsey }
    end

    context "when name is defined" do
      before { stop_area.name = "dummy" }
      it { is_expected.to be_truthy }
    end
  end
end
