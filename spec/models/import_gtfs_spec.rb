require 'rails_helper'

RSpec.describe Import::Gtfs, type: :model do
  context 'setup test environment' do
    it 'should import a file' do
      # No StopAreas exist at this point
      expect(StopArea.count).to eq(0)

      # Initial import of 9 StopAreas
      file_url = 'https://developers.google.com/transit/gtfs/examples/sample-feed.zip'
      source = GTFS::Source.build(file_url)
      ImportWorker.new.perform(source.stops.to_json)
      expect(StopArea.count).to eq(9)
      expect(StopArea.pluck(:external_code).sort).to eq(["AMV", "BEATTY_AIRPORT", "BULLFROG", "DADAN", "EMSI", "FUR_CREEK_RES", "NADAV", "NANAA", "STAGECOACH"])

      # Another StopArea is created with the wrong code
      factory = RGeo::Geographic.spherical_factory(srid: 4326)
      StopArea.create! name: 'invalid_external_code', centroid: factory.point(-90.0, 36.641496), external_code: "AMV1111"
      expect(StopArea.count).to eq(10)

      # Another StopArea is created without external_code
      factory = RGeo::Geographic.spherical_factory(srid: 4326)
      StopArea.create! name: 'no_external_code', centroid: factory.point(-90.0, 36.641496)
      expect(StopArea.count).to eq(11)

      # A cleanup is called right after and the count is back to normal,
      # the StopArea is removed because it's not part of the initial import of file_url
      stops_ids = source.stops.map(&:id)
      CleanupWorker.new.perform(stops_ids.to_json)
      expect(StopArea.only_deleted.map(&:name).sort).to eq(['invalid_external_code', 'no_external_code'])
      expect(StopArea.count).to eq(9)
    end
  end
end
