require 'rgeo'

class ImportWorker
  include Sidekiq::Worker

  sidekiq_options :queue => :default, :retry => true, :backtrace => true

  attr_accessor :factory

  def initialize
    @factory = RGeo::Geographic.spherical_factory(srid: 4326)
  end

  def perform(stops_json)
    Rails.logger.info stops_json.inspect
    stops_attributes = JSON.parse(stops_json)
    stops = stops_attributes.collect { |stop_attributes| GTFS::Stop.new(stop_attributes) }
    stops.each do |stop|
      begin
        Rails.logger.info(stop.inspect)
        stop_area = StopArea.find_or_create_by :external_code => stop.id
        stop_area.name = stop.name
        stop_area.centroid = factory.point(stop.lat, stop.lon)
        stop_area.save!
      rescue StandardError => exception
        Rails.logger.error(exception.message)
        next
      end
    end
  rescue => exception
    Rails.logger.error(exception)
  end
end
