class CleanupWorker
  include Sidekiq::Worker

  sidekiq_options :queue => :default, :retry => true, :backtrace => true

  def perform(stops_ids_json)
    Rails.logger.info stops_ids_json.inspect
    stops_ids = JSON.parse stops_ids_json
    StopArea.where(['external_code IS NULL OR external_code NOT IN (?)', stops_ids]).destroy_all
  rescue => exception
    Rails.logger.error(exception)
  end
end
