# Test import purposes

The goal: create/update/mark as deleted stop areas according a (large) GTFS file

## Context

A (small) StopArea model is defined.

## GTFS stops.txt -> StopArea

* GTFS `stop_name` is `StopArea#name`
* GTFS `stop_id` is `StopArea#external_code`
* GTFS `parent_station` is `StopArea#parent`
* GTFS `stop_lat` and `stop_lon` are `StopArea#centroid`
* GTFS `platform_code` is `StopArea#public_code`

## Constraints

* Update an existing StopArea when external_code matches the stop_id
* Create an existing StopArea when no external_code matches the stop_id
* Mark as deleted a StopArea when no stop_id matches is external_code
* Fill all StopArea attributes according to the 'business' mapping
* Leave unchanged a StopArea attribute when the GTFS stop doesn't provide a value (for example an empty platform_code)
* Avoid invalid models in database

## Objectives

By priority:

* Update performance
* Create performace
* Make readable specs

## Dependencies

https://github.com/rgeo/activerecord-postgis-adapter

```
apt-get install postgis postgresql-14-postgis-3
```

## Import data

```
bundle exec rake "data:import[https://developers.google.com/transit/gtfs/examples/sample-feed.zip]"
```

## Clear data

```
bundle exec rake "data:cleanup[https://developers.google.com/transit/gtfs/examples/sample-feed.zip]"
```

## How to test

```
bundle exec rspec
```

## How to perform an import

```
rake download:sample_sample
# or
rake download:big_sample
```

To perform an import:

```
rake "import[gtfs.zip]"
```
