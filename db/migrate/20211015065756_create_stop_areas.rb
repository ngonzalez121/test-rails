class CreateStopAreas < ActiveRecord::Migration[6.1]
  def change
    create_table :stop_areas do |t|
      t.bigint :parent_id
      t.string :name
      t.string :external_code
      t.string :public_code
      t.datetime :deleted_at
      t.st_point :centroid, geographic: true
      t.index [:external_code]
      t.index [:centroid], using: :gist
      t.timestamps
    end
  end
end
